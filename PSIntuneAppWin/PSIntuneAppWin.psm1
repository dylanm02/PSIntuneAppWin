# Set variables with corresponding values based on the OS of the computer
if ($PSVersionTable.PSEdition -eq 'Desktop') {

    $PWSHIsWindows = $true;

} else {

    $PWSHIsWindows = $IsWindows;

}

# Check and do not allow module import if running on a windows device
if (!$PWSHIsWindows) {

    throw "This module can only be used on Windows device. Please install this module on a Windows device and try again."

}

#Get public and private function definition files.
$Public  = @( Get-ChildItem -Path $PSScriptRoot\Public\*.ps1 -ErrorAction SilentlyContinue )
$Private = @( Get-ChildItem -Path $PSScriptRoot\Private\*.ps1 -ErrorAction SilentlyContinue )

#Dot source the files
Foreach($import in @($Public + $Private))
{
    Try
    {
        . $import.fullname
    }
    Catch
    {
        Write-Error -Message "Failed to import function $($import.fullname): $_"
    }
}

# Set variables visible to the module and its functions only
$PSModuleRoot = $PSScriptRoot
$IsRunningAsAdministrator = [bool](([System.Security.Principal.WindowsIdentity]::GetCurrent()).groups -match "S-1-5-32-544")

# Export Public functions ($Public.BaseName) for WIP modules
Export-ModuleMember -Function $Public.Basename

# Write message when imported
Write-Host "Before you use this module, please refer to the Microsoft Win32 Content Prep Tool documentation.
You can quickly open the documentation for the Microsoft Win32 Content Prep Tool by using the Show-PSIntuneAppWinDocs cmdlet with the -MSDocs parameter." -ForegroundColor Yellow