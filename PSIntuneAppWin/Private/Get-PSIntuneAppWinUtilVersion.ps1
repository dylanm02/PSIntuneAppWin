function Get-PSIntuneAppWinUtilVersion
{
    [CmdletBinding()]
    Param()

    $process = New-Object System.Diagnostics.Process
    $process.StartInfo.UseShellExecute = $false
    $process.StartInfo.RedirectStandardOutput = $true
    $process.StartInfo.RedirectStandardError = $true
    $process.StartInfo.FileName = "$PSModuleRoot\bin\IntuneWinAppUtil.exe"
    $process.StartInfo.Arguments = "-v"
    $out = $process.Start()
    
    $StandardOutput = $process.StandardOutput.ReadToEnd()
    
    return $StandardOutput.Trim()
}
