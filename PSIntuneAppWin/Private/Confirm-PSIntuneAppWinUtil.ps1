function Confirm-PSIntuneAppWinUtil
{
    Write-Verbose "Checking if the IntuneWinAppUtil has already been downloaded"

    if (Test-Path "$PSModuleRoot\bin\IntuneWinAppUtil.exe") {

        Write-Verbose "IntuneWinAppUtil is downloaded"
        return $true

    } else {

        Write-Verbose "IntuneWinAppUtil is not downloaded"
        return $false

    }

}
