function Get-PSIntuneAppWinUtil
{
    $GitHubRepoURL = "microsoft/Microsoft-Win32-Content-Prep-Tool"
    $GitHubRepoReleasesURL = "https://api.github.com/repos/$GitHubRepoURL/releases"

    $TempPath = "$($env:TEMP)\$((New-Guid).guid)"

    $LatestGithubRelease = (Invoke-WebRequest $GitHubRepoReleasesURL -UseBasicParsing  | ConvertFrom-Json)[0]

    # Create a temp path for the download
    if (!(Test-Path $TempPath)) {
        New-Item -Path $TempPath -ItemType "directory" | Out-Null
    }
    
    # Write-Host "Dowloading latest release"
    Invoke-WebRequest -Uri $LatestGithubRelease.zipball_url -OutFile "$TempPath\LatestDownload.zip" -UseBasicParsing

    #Write-Host Extracting release files
    Expand-Archive "$TempPath\LatestDownload.zip" -DestinationPath $TempPath -Force

    # Delete the the zip folder
    Remove-Item "$TempPath\LatestDownload.zip" -Force | Out-Null

    # Extract the exe that we require
    $ExtractedFolderPath = (Get-ChildItem -Path $TempPath -Directory)[0].FullName

    # Find the exe file
    $IntuneAppWinFilePath = (Get-ChildItem -Path "$ExtractedFolderPath\*" -File -Include *.exe)[0].FullName

    # Create a bin path for the extraction
    if (!(Test-Path "$PSModuleRoot\bin\")) {
        New-Item -Path "$PSModuleRoot\bin\" -ItemType "directory" | Out-Null
    }

    # Move the file to the module bin folder
    Move-Item $IntuneAppWinFilePath -Destination "$PSModuleRoot\bin\IntuneWinAppUtil.exe" -Force

    # Cleaning up temp dir
    Remove-Item $TempPath -Recurse -Force -ErrorAction SilentlyContinue 

}
