function Get-PSIntuneAppWinUtilLastestGithubVersion
{
    [CmdletBinding()]
    Param()

    $GitHubRepoURL = "microsoft/Microsoft-Win32-Content-Prep-Tool"
    $GitHubRepoReleasesURL = "https://api.github.com/repos/$GitHubRepoURL/releases"

    $LatestGithubRelease = (Invoke-WebRequest $GitHubRepoReleasesURL -UseBasicParsing | ConvertFrom-Json)[0]
    
    return $LatestGithubRelease.tag_name

}
