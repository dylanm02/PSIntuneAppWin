<#
.PARAMETER SourcePath
Setup folder for all setup files. All files in this folder will be compressed into .intunewin file. Only the setup files for this app should be in this folder.

If the SourcePath parameter is not specified, the module will use the root directory from where you are running the cmdlet from.

Parameter is not required.

.PARAMETER OutputPath
Output folder for the generated .intunewin file.

Parameter is required.

.PARAMETER SetupFile
Setup file (e.g. setup.exe or setup.msi).

Parameter is required.

.PARAMETER CatalogPath
Catalog folder for all catalog files. All files in this folder will be treated as catalog file for Win10 S mode.

Parameter is not required.

.EXAMPLE
PS> New-PSIntuneAppWin -SourcePath "C:\Source" -SetupFile "Setup.msi" -OutputPath "C:\Output"

.LINK
Online cmdlet documentation: https://gitlab.com/dylanm02/PSIntuneAppWin/-/wikis
#>

function New-PSIntuneAppWin
{
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$false)]
        [ValidateScript({
            if(-Not ($_ | Test-Path) ){
                throw "The file or folder does not exist"
            }
            if(-Not ($_ | Test-Path -PathType Container) ){
                throw "Argument must be a folder. File paths are not allowed."
            }
            return $true  
        })]
        [System.IO.FileInfo]$SourcePath = (Get-Location).Path,

        [Parameter(Mandatory=$true)]
        [ValidateScript({
            if(-Not ($_ | Test-Path) ){
                throw "The file or folder does not exist"
            }
            if(-Not ($_ | Test-Path -PathType Container) ){
                throw "Argument must be a folder. File paths are not allowed."
            }
            return $true  
        })]
        [System.IO.FileInfo]$OutputPath,

        [Parameter(Mandatory=$true)]
        [ValidateScript({
            if(-Not ("$SourcePath\$_" | Test-Path) ){
                throw "The file or folder does not exist"
            }
            if(-Not ("$SourcePath\$_" | Test-Path -PathType Leaf) ){
                throw "The SetupFile argument must be a file. Folder paths are not allowed."
            }
            return $true  
        })]
        [String]$SetupFile,

        [Parameter(Mandatory=$false)]
        [ValidateScript({
            if(-Not ($_ | Test-Path) ){
                throw "The file or folder does not exist"
            }
            if(-Not ($_ | Test-Path -PathType Container) ){
                throw "Argument must be a folder. File paths are not allowed."
            }
            return $true  
        })]
        [System.IO.FileInfo]$CatalogPath
    )

    if (!(Confirm-PSIntuneAppWinUtil)) {

        Get-PSIntuneAppWinUtil

    } else {

        $DownloadedVerison = Get-PSIntuneAppWinUtilVersion
        $LastestGithubVersion = Get-PSIntuneAppWinUtilLastestGithubVersion

        if (!(($DownloadedVerison -eq $LastestGithubVersion) -or ($DownloadedVerison -eq "$LastestGithubVersion.0") -or ($DownloadedVerison -eq "v$LastestGithubVersion") -or ($DownloadedVerison -eq "v$LastestGithubVersion.0"))) {

            Get-PSIntuneAppWinUtil

        }

    }

    $SourcePathArg = '-c "' + "$(($SourcePath).FullName)" + '"'
    $SetupFileArg = '-s "' +  $SetupFile + '"'
    $OutputPathArg = '-o "' +  ($OutputPath).FullName + '"'

    if (($CatalogPath -ne '') -or ($CatalogPath -ne $null)) {

        $ProcessArgs = @(
            "-q",
            $SourcePathArg,
            $SetupFileArg,
            $OutputPathArg
        )

    } else {

        $CatalogPathArg = '-c "' +  ($CatalogPath).FullName + '"'

        $ProcessArgs = @(
            "-q",
            $SourcePathArg,
            $SetupFileArg,
            $OutputPathArg
            $CatalogPathArg
        )

    }

    Start-Process -FilePath "$PSModuleRoot\bin\IntuneWinAppUtil.exe" -ArgumentList $ProcessArgs -Wait -NoNewWindow

}
