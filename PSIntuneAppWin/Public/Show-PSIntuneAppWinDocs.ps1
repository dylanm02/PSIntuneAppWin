<#
.SYNOPSIS
Opens the documentation.

.DESCRIPTION
The Show-PSIntuneAppWinDocs cmdlet opens the online documentation within the default web browser.

.PARAMETER MSDocs
Flag to change the documentation from the module documentation to the Microsoft documentation.

Parameter is not required.

.EXAMPLE
PS> Show-PSIntuneAppWinDocs

.EXAMPLE
PS> Show-PSIntuneAppWinDocs -MSDocs

.INPUTS
None. You cannot pipe objects to Show-PSIntuneAppWinDocs

.OUTPUTS
None. Show-PSIntuneAppWinDocs does not generate any output.

.LINK
Online cmdlet documentation: https://gitlab.com/dylanm02/PSIntuneAppWin/-/wikis
#>

function Show-PSIntuneAppWinDocs
{
    [CmdletBinding()]
    Param(
        [switch]$MSDocs = $False
    )

    if ($MSDocs) {
        Start-Process "https://github.com/microsoft/Microsoft-Win32-Content-Prep-Tool"
    } else {
        Start-Process "https://gitlab.com/dylanm02/PSIntuneAppWin"
    }
    
}
