
# PSIntuneAppWin
![GitLab Release (latest by date)](https://img.shields.io/gitlab/v/release/35580550) ![PowerShell Gallery Version](https://img.shields.io/powershellgallery/v/PSIntuneAppWin) 

A simple Powershell module that creates a wrapper for the [Microsoft Win32 Content Prep Tool](https://github.com/microsoft/Microsoft-Win32-Content-Prep-Tool). This module will automatically check for, download and use the latest version of the Microsoft Win32 Content Prep Tool.

Use the `New-PSIntuneAppWin` cmdlet to pre-process Windows Classic apps. The cmdlet converts application installation files into the .intunewin format. The cmdlet also detects the parameters required by Intune to determine the application installation state. After you use this cmdlet on your apps, you will be able to upload and assign the apps in the Microsoft Intune console.

## Documentation
Documentation on the PSIntuneAppWin module can be found on our [Wiki pages](https://gitlab.com/dylanm02/PSIntuneAppWin/-/wikis/home).

## Installation
### From the PowerShell Gallery

Installing items from the Gallery requires the latest version of the PowerShellGet module, which is available in Windows 10, in Windows Management Framework (WMF) 5.0, or in the MSI-based installer (for PowerShell 3 and 4).

Open Powershell and run the following command:

```PowerShell tab=
PS> Install-Module -Name PSIntuneAppWin
```

### From the GitLab release page

_**This is usually the same as the Powershell Gallery version**_

1. [Click here](https://gitlab.com/dylanm02/PSIntuneAppWin/-/releases) to go to the latest releases, then download the *PSIntuneAppWin.zip* file attached to the release
2. Right-click the downloaded zip, select Properties, then unblock the file.
    _This is to prevent having to unblock each file individually after unzipping._
3. Unzip the archive.
4. (Optional) Place the module somewhere in your PSModulePath.
    * You can view the paths listed by running the environment variable `$env:PSModulePath`

## Support
Need support? No problem, just [raise an issue](https://gitlab.com/dylanm02/PSIntuneAppWin/-/issues/new). When creating a support issue, please add as much information as possible (code snippets, error messages, etc) and remember to apply the 'support' tag to your issue.

## Roadmap
We currently have a roadmap that is freely available on our [milestones page](https://gitlab.com/dylanm02/PSIntuneAppWin/-/milestones).

## Contributing
We are currently not open to code contributions however we are open to suggestions and feature requests. If you would like to see any particular features or have any suggestions, please feel free to [raise an issue](https://gitlab.com/dylanm02/PSIntuneAppWin/-/issues/new) with the 'suggestion' tag. 

## Authors and acknowledgment
This module depends on the [Microsoft Win32 Content Prep Tool](https://github.com/microsoft/Microsoft-Win32-Content-Prep-Tool) that is authored and managed by Microsoft Corporation.

## License
This project is licensed under the GNU General Public License v3.0. For more information on the GNU General Public License v3.0, please read the [LICENSE.md](https://gitlab.com/dylanm02/PSIntuneAppWin/-/blob/main/LICENSE) file.

## Project status
This project is in active development and new features are added as and when they are needed. If you would like to see any particular features or have any suggestions, please feel free to [raise an issue](https://gitlab.com/dylanm02/PSIntuneAppWin/-/issues/new) with the 'suggestion' tag.