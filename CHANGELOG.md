# Release Notes

### Version 1.0.2 - Update spelling mistakes

Updated spelling mistakes in the psd1 and readme files

### Version 1.0.1 - Update to psm1 tags

Updated the psm1 tags to allow import into powershell gallery

### Version 1.0.0 - Initial Release

Initial release of the PSIntuneAppWin module

---

PSIntuneAppWin includes a variety of changes. Please consult the diff to see what's new.